�
���Tc           @   sY   d  Z  d d l Z d d l Z dZ de f d�  �  YZ e �  Z e j Z e j Z d S(  s�   
humanhash: Human-readable representations of digests.

The simplest ways to use this module are the :func:`humanize` and :func:`uuid`
functions. For tighter control over the output, see :class:`HumanHasher`.
i����Nt   ackt   alabamat   alaninet   alaskat   alphat   angelt   apartt   aprilt   arizonat   arkansast   artistt	   asparagust   aspent   augustt   autumnt   avocadot   bacont   bakerloot   batmant   beert   berlint	   berylliumt   blackt   blossomt   bluet   bluebirdt   bravot   bulldogt   burgert   buttert
   californiat   carbont   cardinalt   carolinat   carpett   catt   ceilingt   charliet   chickent   coffeet   colat   coldt   coloradot   comett   connecticutt   crazyt   cupt   dakotat   decembert   delawaret   deltat   diett   dont   doublet   earlyt   eartht   eastt   echot   edwardt   eightt   eighteent   elevent   emmat   enemyt   equalt   failedt   fantat   fifteent   fillett   fincht   fisht   fivet   fixt   floort   floridat   footballt   fourt   fourteent   foxtrott   freddiet   friendt   fruitt   geet   georgiat   glucoset   golft   greent   greyt   hampert   happyt   harryt   hawaiit   heliumt   hight   hott   hotelt   hydrogent   idahot   illinoist   indiat   indigot   inkt   iowat   islandt   itemt   jerseyt   jigt   johnnyt   juliett   julyt   jupitert   kansast   kentuckyt   kilot   kingt   kittent   lactoset   laket   lampt   lemont   leopardt   limat   liont   lithiumt   londont	   louisianat   lowt   magazinet	   magnesiumt   mainet   mangot   marcht   marst   marylandt   massachusettst   mayt   mexicot   michigant   miket	   minnesotat   mirrort   mississippit   missourit   mobilet   mockingbirdt   monkeyt   montanat   moont   mountaint   muppett   musict   nebraskat   neptunet   networkt   nevadat   ninet   nineteent   nitrogent   northt   novembert   nutst   octobert   ohiot   oklahomat   onet   oranget   orangest   oregont   oscart   ovent   oxygent   papat   parist   pastat   pennsylvaniat   pipt   pizzat   plutot   potatot   princesst   purplet   quebect   queent   quiett   redt   rivert   robertt   robint   romeot   rugbyt   sadt   salamit   saturnt	   septembert   sevent	   seventeent   shadet   sierrat   singlet   sinkt   sixt   sixteent   skylarkt   snaket   socialt   sodiumt   solart   southt	   spaghettit   speakert   springt   stairwayt   steakt   streamt   summert   sweett   tablet   tangot   tent	   tennesseet   tennist   texast   thirteent   threet   timingt   triplet   twelvet   twentyt   twot   unclet   undresst   uniformt   uranust   utaht   vegant   venust   vermontt   victort   videot   violett   virginiat
   washingtont   westt   whiskeyt   whitet   williamt   winnert   wintert	   wisconsint   wolframt   wyomingt   xrayt   yankeet   yellowt   zebrat   zulut   HumanHasherc           B   sA   e  Z d  Z e d � Z d d d � Z e d �  � Z d �  Z RS(   s�  
    Transforms hex digests to human-readable strings.

    The format of these strings will look something like:
    `victor-bacon-zulu-lima`. The output is obtained by compressing the input
    digest to a fixed number of bytes, then mapping those bytes to one of 256
    words. A default wordlist is provided, but you can override this if you
    prefer.

    As long as you use the same wordlist, the output will be consistent (i.e.
    the same digest will always render the same representation).
    c         C   s.   t  | � d k r! t d � � n  | |  _ d  S(   Ni   s$   Wordlist must have exactly 256 items(   t   lent   ArgumentErrort   wordlist(   t   selfR  (    (    s1   /home/jason/Development/preposterous/humanhash.pyt   __init__B   s    i   t   -c      
      sp   t  d �  t  d j t | d d d � | d d d � � � � } �  j | | � } | j �  f d �  | D� � S(   s?  
        Humanize a given hexadecimal digest.

        Change the number of words output by specifying `words`. Change the
        word separator with `separator`.

            >>> digest = '60ad8d0d871b6095808297'
            >>> HumanHasher().humanize(digest)
            'sodium-magnesium-nineteen-hydrogen'
        c         S   s   t  |  d � S(   Ni   (   t   int(   t   x(    (    s1   /home/jason/Development/preposterous/humanhash.pyt   <lambda>U   s    t    Ni   i   c         3   s   |  ] } �  j  | Vq d  S(   N(   R  (   t   .0t   byte(   R  (    s1   /home/jason/Development/preposterous/humanhash.pys	   <genexpr>Z   s    (   t   mapt   joint   zipt   compress(   R  t	   hexdigestt   wordst	   separatort   bytest
   compressed(    (   R  s1   /home/jason/Development/preposterous/humanhash.pyt   humanizeG   s    	8c         C   s�   t  |  � } | | k r' t d � � n  | | } g  t | � D] } |  | | | d | !^ q> } | d j |  | | � d �  } t | | � } | S(   s  
        Compress a list of byte values to a fixed target length.

            >>> bytes = [96, 173, 141, 13, 135, 27, 96, 149, 128, 130, 151]
            >>> HumanHasher.compress(bytes, 4)
            [205, 128, 156, 96]

        Attempting to compress a smaller number of bytes to a larger number is
        an error:

            >>> HumanHasher.compress(bytes, 15)  # doctest: +ELLIPSIS
            Traceback (most recent call last):
            ...
            ValueError: Fewer input bytes than requested output
        s'   Fewer input bytes than requested outputi   i����c         S   s   t  t j |  d � S(   Ni    (   t   reducet   operatort   xor(   R  (    (    s1   /home/jason/Development/preposterous/humanhash.pyR	  {   s    (   R  t
   ValueErrort   xranget   extendR  (   R  t   targett   lengtht   seg_sizet   it   segmentst   checksumt	   checksums(    (    s1   /home/jason/Development/preposterous/humanhash.pyR  \   s    
/	c         K   s4   t  t j �  � j d d � } |  j | | � | f S(   s�   
        Generate a UUID with a human-readable representation.

        Returns `(human_repr, full_digest)`. Accepts the same keyword arguments
        as :meth:`humanize` (they'll be passed straight through).
        R  R
  (   t   strt   uuidlibt   uuid4t   replaceR  (   R  t   paramst   digest(    (    s1   /home/jason/Development/preposterous/humanhash.pyt   uuid   s    	(	   t   __name__t
   __module__t   __doc__t   DEFAULT_WORDLISTR  R  t   staticmethodR  R*  (    (    (    s1   /home/jason/Development/preposterous/humanhash.pyR   3   s
   #(   R    R   R   R   s   alphaR   R   s   aprilR   R	   R
   R   R   s   augustR   R   R   R   R   R   R   R   R   R   R   R   R   R   R   R   R   R   R    R!   R"   s   catR$   R%   R&   R'   R(   R)   R*   R+   R,   R-   R.   R/   s   decemberR1   R2   R3   R4   R5   R6   R7   R8   R9   R:   R;   R<   R=   R>   R?   R@   RA   RB   RC   RD   RE   RF   RG   RH   s   floorRJ   RK   RL   RM   RN   RO   RP   RQ   RR   RS   RT   RU   RV   RW   RX   RY   RZ   R[   R\   s   highR^   R_   R`   Ra   Rb   Rc   Rd   Re   Rf   Rg   s   itemRi   Rj   Rk   Rl   s   julyRn   Ro   Rp   Rq   Rr   Rs   Rt   Ru   Rv   Rw   Rx   Ry   Rz   R{   R|   R}   s   lowR   R�   R�   R�   s   marchR�   R�   R�   s   mayR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   s   novemberR�   s   octoberR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   s	   septemberR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   s   streamR�   R�   s   tableR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   s   uniformR�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   (	   R-  R  R*  R%  R.  t   objectR   t   DEFAULT_HASHERR  (    (    (    s1   /home/jason/Development/preposterous/humanhash.pyt   <module>   sT                                      Y		